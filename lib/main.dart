import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
    home: new MyApp(),
  ));
} 

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Example App"),
        backgroundColor: Colors.blue,
      ),
      backgroundColor: Colors.blue,
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Icon(Icons.favorite, color: Colors.redAccent, size: 200.0,
            ),
          ],
        ),
      ),
    );
  }
}